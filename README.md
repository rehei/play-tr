# What is it about?

Play-tr is a module for the play! framework 1.2.5 and is supposed to support you when translating your application into different languages (i18n/l10n).

# Configuration

Play-tr takes necessary configuration from the application.conf of your application. 

e.g. application.langs 

# Commands
## tr:regen

For each of your application languages, it will look for files in {application}/conf/data/, combines them and stores them to a application/conf/messages.{language} file.

Assume the following setup:

1) In your application.conf you find a property:

application.langs=en,sv

2) In conf/data/ there are the following files:

./conf/data/events.en
./conf/data/events.sv
./conf/data/home.en
./conf/data/home.sv 

3) Type *tr:regen*

Two new files are created:

./conf/messages.en
./conf/messages.sv

And they contain all the keys of the respective language.

4) A warning is shown if the files (e.g. events.en, events.sv) do not contain the same amount of keys. 
