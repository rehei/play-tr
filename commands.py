import os, os.path
import sys
import shutil
import subprocess
from string import *
from sets import *

MODULE = 'tr'
COMMANDS = ['tr:regen']

def execute(**kargs):
    command = kargs.get("command")
    app = kargs.get("app")
    args = kargs.get("args")
    env = kargs.get("env")

    langs = app.conf.entries.get("application.langs").split(",")

    if command == 'tr:regen':
        confPath = "%s/%s/conf/" % (env["basedir"], app.name())
        dataPath = confPath + "data/"
        for lang in langs :
            try:
                os.remove(confPath + "messages." + lang)
            except:
                pass

        filenames = Set();        

        for filename in os.listdir(dataPath):
            (name, lang) = filename.rsplit(".")
	    filenames.add(name); 

        for filename in filenames :
            count = {};
            for lang in langs:
                dest = open(confPath + "messages." + lang , "a")
                source = open(dataPath + filename + "." + lang, "r")
                
                count[lang] = 0;
                for line in source.readlines() :
                    if(line is not None and len(line.strip())>0) :
                        count[lang] = count[lang] + 1;
                        dest.write(line)
                
                source.close()
                dest.write("\n\n")
                dest.close() 
        
            last = None
            lastMatched = True
            for lang in langs:
                if(count[lang] == last or last is None) :
                    lastMatched = lastMatched and True
                else :
                    lastMatched = lastMatched and False
                last = count[lang]
            
            if(lastMatched is False) :
                print("Warning: Incorrect linecount for " + filename);
                
               
                          
           
    
